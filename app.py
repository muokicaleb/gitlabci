from flask import Flask
from flask_restful import Api, Resource, request

app = Flask(__name__)
api = Api(app)

class BmiCalc(Resource):
    def post(self):
        json_data = request.json

        height = json_data["height"]
        weight = json_data["weight"]
        bmi = weight / (height/100)**2

        # new change  
        rounded_bmi = round(bmi, 2 ) 
        return {'bmi': rounded_bmi, "msg":"v5"}

class IngressTest1(Resource):
    def get(self):
        return {"msg":"from ingress test1"}

class IngressTest2(Resource):
    def get(self):
        return {"msg":"from ingress test2"}

api.add_resource(BmiCalc, '/bmicalc')
api.add_resource(IngressTest1, '/bmicalc/test1')
api.add_resource(IngressTest2, '/bmicalc/test2')

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0', port=5001)
